Capture events from input elements outside the visualization.
Similar to what is done at min 3:19 in the [Reactive Vega](http://idl.cs.washington.edu/papers/reactive-vega) video

<iframe class="player" src="//player.vimeo.com/video/100936827" width="630" height="354" frameborder="0" webkitallowfullscreen="1" mozallowfullscreen="1" allowfullscreen="1"></iframe>